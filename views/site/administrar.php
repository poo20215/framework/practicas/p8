<?php

use yii\grid\GridView;
use yii\bootstrap4\Html;

echo GridView::widget([
    'dataProvider' => $dataProvider,
    'layout' =>"{items}", //para evitar que salga Mostrando 1-4 de 4 elementos.
    'columns' => [
        'id',
        'descripcion',
        'procesador',
        'memoria',
        'discoduro',
        [
            'attribute'=>'ethernet',
            'format'=>'raw',
            'content'=>function($model){
                //return Html::activeCheckbox($model, "ethernet",['disabled'=>true]);
                if($model->ethernet)
                {
                    return '<i class="far fa-check-square"></i>';
                }else
                {
                    return '<i class="far fa-square"></i>';
                }
                
            }
        ],
        [
            'attribute'=>'wifi',
            'format'=>'raw',
            'value'=>function($data){
                if($data->wifi==1)
                {
                    return '<i class="far fa-check-square"></i>';
                }else
                {
                    return '<i class="far fa-square"></i>';
                }
                
            }
        ],
        'video',
                
        [
            'header' => 'Acciones',
            'headerOptions' => ['class' => 'btn-link font-weight-bold'],
            'class' => 'yii\grid\ActionColumn',
                'template' => '{delete} {update} {view} {new}',  //orden de los botones    
                'buttons' => [
                'view' => function($url,$model){
                return Html::a('<i class="fas fa-eye"></i>',['site/ver','id' => $model->id]);
                },
                'update' => function($url,$model){
                    return Html::a('<i class="fas fa-pencil-alt"></i>',['site/actualizar','id' => $model->id]);
                    },
                'delete' => function($url,$model){
                    return Html::a('<i class="fas fa-trash-alt"></i>',['site/eliminar','id' => $model->id]);
                    }, 
                'new' => function($url,$model){
                    return Html::a('<i class="fas fa-plus"></i>',['site/anadir','id' => $model->id]);
                    },
                            
            ]],
    ]]);

?>