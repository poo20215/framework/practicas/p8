<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use kartik\switchinput\SwitchInput;

/* @var $this yii\web\View */
/* @var $model app\models\Ordenadores */
/* @var $form ActiveForm */
?>
<div class="formulario">
    
    <h4><u><?= $titulo ?></u></h4>

<?php
$opciones=[
    "template"=> '{input}{label}',
    "inputOptions" => ['class' => 'col-lg-9'],
    "labelOptions" => ['class' => 'col-lg-2'],
    "options" => ["class" => "row mb-2"],
];

$opciones1=[
            'template' => '{label}{input}',
            'inputOptions' => ['class'=>'col-lg-9 p-0'],
            'labelOptions' => ['class'=>'col-lg-1 p-0'],
            'options' => ['form-check form-inline']
        ];

?>

    <?php $form = ActiveForm::begin(); ?>
        
        <?= $form->field($model, 'descripcion',$opciones)->textarea(["rows"=>6]) ?>
        <?= $form->field($model, 'procesador',$opciones) ?>
        <?= $form->field($model, 'memoria',$opciones) ?>
        <?= $form->field($model, 'discoduro',$opciones) ?>
        <?php //$form->field($model, 'ethernet',$opciones1)->checkbox(['class'=>'form-check-input'],false) ?>
        <?= $form->field($model, 'ethernet')->widget(SwitchInput::classname(),['pluginOptions' => ['onText' => 'Con Ethernet', 'offText' => 'Sin Ethernet']])->label('')?>
        <?= $form->field($model, 'wifi')->widget(SwitchInput::classname(),['pluginOptions' => ['onText' => 'Con Wifi', 'offText' => 'Sin Wifi']])->label('') ?>
        <?= $form->field($model, 'video',$opciones) ?>
    
        <div class="form-group">
            <?= Html::submitButton($accion, ['class' => 'btn btn-primary']) ?>
        </div>
    <?php ActiveForm::end(); ?>

</div><!-- formulario -->
