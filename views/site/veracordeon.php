<?php
use yii\bootstrap4\Accordion;
use yii\helpers\Html;

echo Accordion::widget([
    'items' => [
        // equivalent to the above
        [
            'label' => $model->getAttributeLabel("descripcion"),
            'content' => $model->descripcion,
            'expand' => true,
        ],
        // another group item
        [
            'label' => $model->getAttributeLabel("procesador"),
            'content' => $model->procesador,
        ],
        [
            'label' => $model->getAttributeLabel("memoria"),
            'content' => $model->memoria,
        ],
        [
            'label' => 'Red',
            'content' => $red,
        ],
        // if you want to swap out .card-block with .list-group, you may use the following
        [
            'label' => $model->getAttributeLabel("video"),
            'content' => $model->video,
        ],
    ]
]);


echo "<br>";        
echo Html::a("VER NORMAL",["site/ver",'id' => $model->id], ['class' => 'btn btn-primary']); 