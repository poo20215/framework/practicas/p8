<?php

use yii\widgets\ListView;

echo ListView::widget([
    "dataProvider" => $dataProvider,
    "itemView" => '_listar',
    "options" => [
        "class" => "row"
    ],
    "itemOptions" => [
        "class" => "col-lg-3 border m-2 p-2",
    ],
    ]);

?>



