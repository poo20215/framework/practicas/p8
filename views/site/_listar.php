<?php
use yii\helpers\Html;
use app\widgets\Modal;
?>

<table>
    <tr><td colspan="2"><?= $model->id?></td><td><?= substr($model->descripcion,0,10) . "..." ?><td></tr>
    <tr><td><?= Html::a('<i class="fas fa-plus-circle"></i>',['site/ver','id' => $model->id],['class' => 'botonModal']) ?></td></tr>
</table>

<?php
echo Modal::widget();
?>