<?php
use yii\widgets\DetailView;
use yii\helpers\Html;
?>

<h4><u>DATOS DEL ORDENADOR</u></h4>

<?php
echo DetailView::widget([
    'model' => $model,
    'attributes' => [
        'id',
        'descripcion',
        'procesador',
        'memoria',
        'discoduro',
        [
            'attribute'=>'ethernet',
            'format'=>'raw',
            'value'=>function($data){
                if($data->ethernet==1)
                {
                    return '<i class="far fa-check-square"></i>';
                }else
                {
                    return '<i class="far fa-square"></i>';
                }
                
            }
        ],
        [
            'attribute'=>'wifi',
            'format'=>'raw',
            'value'=>function($data){
                if($data->wifi==1)
                {
                    return '<i class="far fa-check-square"></i>';
                }else
                {
                    return '<i class="far fa-square"></i>';
                }
                
            }
        ],
        'video',
            ]
]);

echo "<br>";        
echo Html::a("VER POR BLOQUES",["site/veracordeon",'id' => $model->id], ['class' => 'btn btn-primary']) 
        
?>