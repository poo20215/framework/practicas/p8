<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "ordenadores".
 *
 * @property int $id
 * @property string|null $descripcion
 * @property string|null $procesador
 * @property string|null $memoria
 * @property string|null $discoduro
 * @property int|null $ethernet
 * @property int|null $wifi
 * @property string|null $video
 */
class Ordenadores extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'ordenadores';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['ethernet', 'wifi'], 'integer'],
            [['descripcion'], 'string', 'max' => 800],
            [['procesador', 'memoria', 'discoduro', 'video'], 'string', 'max' => 100],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'descripcion' => 'Descripcion',
            'procesador' => 'Procesador',
            'memoria' => 'Memoria RAM',
            'discoduro' => 'Disco duro',
            'ethernet' => 'Tarjeta de red Ethernet',
            'wifi' => 'Tarjeta de red Wifi',
            'video' => 'Tarjeta Video',
        ];
    }
}
