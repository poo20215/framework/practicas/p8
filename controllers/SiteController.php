<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\LoginForm;
use app\models\ContactForm;
use yii\data\ActiveDataProvider;
use app\models\Ordenadores;

class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'only' => ['logout'],
                'rules' => [
                    [
                        'actions' => ['logout'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post'],
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
            'captcha' => [
                'class' => 'yii\captcha\CaptchaAction',
                'fixedVerifyCode' => YII_ENV_TEST ? 'testme' : null,
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        return $this->render('index');
    }
    
    public function actionAdministrar()
    {
        $dataProvider = new ActiveDataProvider([
        'query' => Ordenadores::find()]);
        return $this->render("administrar",["dataProvider" => $dataProvider]);
    }
    
    public function actionAnadir()
    {
        $model = new Ordenadores();
        $titulo="AÑADIR ORDENADOR";
        $accion="Añadir";
        
        if($this->request->isPost) //si ya hay datos del formulario
        {
            if ($model->load(Yii::$app->request->post())) {
                if ($model->save()) 
                {
                    return $this->redirect(["site/administrar"]);
                }
            }
        }
        return $this->render('formulario', [
            'model' => $model,
            'titulo' => $titulo,
            'accion' => $accion,
        ]);
    }
    
    public function actionActualizar($id)
    {
        $model = Ordenadores::findOne($id);
        $titulo="ACTUALIZAR DATOS DEL ORDENADOR";
        $accion="Actualizar";

        if ($model->load(Yii::$app->request->post())) {
            if ($model->save()) 
            {
                return $this->redirect(["site/administrar"]);
            }
        }

        return $this->render('formulario', [
            'model' => $model,
            'titulo' => $titulo,
            'accion' => $accion,
        ]);
    }
    
    public function actionEliminar($id)
    {
        $model = Ordenadores::findOne($id);
        $titulo="<i class='fas fa-trash fa-5x'></i> ELIMINAR ORDENADOR";
        $accion="Eliminar";

        if ($model->load(Yii::$app->request->post())) {
            $model->delete();
            return $this->redirect(["site/administrar"]);
            
        }

        return $this->render('formulario', [
            'model' => $model,
            'titulo' => $titulo,
            'accion' => $accion,
        ]);
    }
    
    public function actionListar()
    {
        $dataProvider = new ActiveDataProvider([
        'query' => Ordenadores::find(),
            'pagination' => [
        'pageSize' => 6,
        ],
            ]);
        return $this->render("listar",["dataProvider" => $dataProvider]);
    }
    
    public function actionVer($id)
    {
        $model= Ordenadores::findOne($id);
        return $this->render("ver",["model"=> $model]);
    }
    
    public function actionVeracordeon($id)
    {
        $red="";
        $model= Ordenadores::findOne($id);
        if($model->ethernet && $model->wifi)
        {
            $red=["Ethernet", "Wifi"];
        }elseif($model->ethernet && !$model->wifi)
        {
            $red="Ethernet";
        }elseif(!$model->ethernet && $model->wifi)
        {
            $red="Wifi";
        }
        return $this->render("veracordeon",["model"=> $model,"red"=> $red]);
    }
}
